#include <stdio.h>
#include <math.h>
#include "my402list.h"
#include "cs402.h"
#include <pthread.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <ctype.h>


static int nTokens = 0, nDroppedTokens = 0, nDroppedPackets=0,TokenNumber=0;
static int packetNumber = 0;
static int totalpackets, nRemainingPackets;
double tTime=0.0, start;
double tq1toq2, tq2toS;
FILE *fp = NULL;
int packetExitFlag = FALSE;
int tokenExitFlag = FALSE;
int flag = FALSE;

void *param;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cv = PTHREAD_COND_INITIALIZER;
int t_argc; char** t_argv;
My402List* queue1, *queue2;
sigset_t set;
pthread_t packetThread, tokenThread, serverThread1, serverThread2, ctrlThread;


double packetRate_cmd = 1; //lambda
int bucketDepth_cmd = 10;//B int
int tokenPerPacket_cmd = 3;//P int
int totalPackets_cmd = 20;//n int
double serviceRate_cmd = 0.35; //mu
double tokenRate_cmd = 1.5; //r

/*statistics*/
double avgTimeQ1;
double avgTimeQ2;
double avgTimeS1;
double avgTimeS2;
double tInterArrivalStatistics, tServiceTimeStatistics, tSystemTimeStatistics, squaredtSystemTimeStatistics;


typedef struct packetInfo {
	int packetNumber;
	int nTokens;
	double pArrivalTime;
	double pServiceTime;
	double packetTime;
	double serverTime;
	double arrivalTime;
} packetInfo;


int checkValidNumber(const char* str) {
	while(*str != '\0') {
		if(!isdigit(*str)) {
	        fprintf(stderr, "\nmalformed command");
	        fprintf(stderr, "\nusage: ./warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n");
			exit(-1);
			return FALSE;
		}
		str++;
	}
	return TRUE;
}


static void cleanup_unlock_mutex(void *p)
{
    pthread_mutex_unlock(p);
}


/*return current time in milliseconds*/
double current_timestamp() {
	struct timeval  tv;
	gettimeofday(&tv, NULL);

	double time_in_mill = (tv.tv_sec) * 1000000.0 + (tv.tv_usec);
    return time_in_mill/1000.0;
}


double current_time() {
	return current_timestamp()-start;
}


void fileReadLine(char *buf, int packetNumber) {

	int nTokensReq;
	double tServiceReq;
	float tInterArrival;

	packetInfo *packetData = (packetInfo*)malloc(sizeof(packetInfo));

	/*do the operations only if a file is given as input*/
	if(fp != NULL) {

		char *start_ptr = buf;
		char *tabptr = start_ptr;
        while(!isdigit(*tabptr))
        {
            tabptr++;
        }
        start_ptr = tabptr;
        while(isdigit(*tabptr))
        {
            tabptr++;
        }
        *tabptr='\0';
        if(tabptr == NULL) {
        			fprintf(stderr, "Input file is not in the right format\n");
        			fprintf(stderr, "usage: ./warmup1 sort [tfile]\n");
        			exit(-1);
        		}
		tInterArrival = atoi(start_ptr);

		//Number of required tokens for the packet

        while(!isdigit(*tabptr))
        {
            tabptr++;
        }
        start_ptr = tabptr;
        while(isdigit(*tabptr))
        {
            tabptr++;
        }

        *tabptr='\0';

		nTokensReq = atoi(start_ptr);

        while(!isdigit(*tabptr))
        {
            tabptr++;
        }
        start_ptr = tabptr;
        while(isdigit(*tabptr))
        {
            tabptr++;
        }
        *tabptr='\0';
		tServiceReq = atoi(start_ptr);
	}
	else {/* for command line input*/
		tInterArrival = 1/packetRate_cmd*1000;
		nTokensReq = tokenPerPacket_cmd;
		tServiceReq = 1/serviceRate_cmd*1000;
	}
	packetData->packetNumber = packetNumber;
	packetData->nTokens = nTokensReq;
	packetData->pServiceTime = tServiceReq;
	param = packetData;

	//Sleep for Inter arrival time period
	//tTotalService = current_time();
	usleep(tInterArrival*1000);
	packetData->arrivalTime = current_time();

	//Packet arrives
	tTime = current_time() - tTime;


	if(nTokensReq > bucketDepth_cmd) {
		nDroppedPackets++;
		printf("\n%012.3f: p%d arrives, needs %d tokens, inter-arrival time = %.3fms, dropped",current_time(), packetData->packetNumber, packetData->nTokens, tTime);
		tInterArrivalStatistics += tTime;
		tTime = current_time();
		return;
	}
	else {
		printf("\n%012.3f: p%d arrives, needs %d tokens, inter-arrival time = %.3fms",current_time(), packetData->packetNumber, packetData->nTokens, tTime);
		tInterArrivalStatistics += tTime;
		tTime = current_time();
	}

    //lock the mutex
    pthread_mutex_lock(&mutex);
    pthread_cleanup_push(cleanup_unlock_mutex, &mutex);


    //Append the packet object to the queue1
    My402ListAppend(queue1, packetData);
    packetData->packetTime = current_time();
    printf("\n%012.3f: p%d enters Q1",current_time(), packetData->packetNumber);

	if(!My402ListEmpty(queue1)) {
		if(nTokens >= packetData->nTokens) { //put the packet into Q2 and decrement the number of tokens

			My402ListUnlink(queue1, My402ListFirst(queue1));       //Remove the element from Q1

			nTokens = nTokens-packetData->nTokens;
			double tempCurrTime1 = current_time();
			printf("\n%012.3f: p%d leaves Q1, time in Q1 = %.3fms, token bucket now has %d tokens", current_time(), packetData->packetNumber, tempCurrTime1-packetData->packetTime, nTokens);


			//statics for avg num of packets in q1

			avgTimeQ1 += (tempCurrTime1- packetData->packetTime);


			My402ListAppend(queue2, packetData);  //Insert the element to Q2

			packetData->serverTime = current_time();
			printf("\n%012.3f: p%d enters Q2",current_time(), packetData->packetNumber);

			pthread_cond_broadcast(&cv); //broadcast to all that a packet is added to Q2
		}
	}



    //unlock the mutex
	pthread_cleanup_pop(1);


}





/*command line parsing if no file is specified*/
void cmdLineParse(int argc, char** argv) {

	char fileName[50], buf[1026];;
	memset(fileName, '\0', sizeof(fileName));
	strcpy(fileName, "NULL");
	int i;

	for(i=1; i<argc; i = i+2) {
		if(strcmp(argv[i],"-t") == 0) {
			fp = fopen(argv[i+1],"r");
			strcpy(fileName, argv[i+1]);


			if (fp == NULL) {
		       fprintf(stderr, "\nInput file %s cannot be opened: %s", argv[2], strerror(errno));
		       fprintf(stderr, "\nusage: ./warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n");
		       exit(EXIT_FAILURE);
		    }

		    struct stat statbuf;
		    stat(argv[2], &statbuf);

		    if(S_ISDIR(statbuf.st_mode)) {
		        fprintf(stderr, "\nInput file %s is a directory", argv[2]);
		        fprintf(stderr, "\nusage: ./warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n");
		        exit(-1);
		    }


			if(fp != NULL && fgets(buf,sizeof(buf),fp) != NULL) {
				if(!flag) { //read first line
					totalpackets = atoi(buf);
					totalPackets_cmd = totalpackets;
					if(totalPackets_cmd == 0){
				        fprintf(stderr, "\nInput file is not in right format");
				        fprintf(stderr, "\nusage: ./warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n");
				        exit(-1);
					}
					flag = TRUE;
				}
			}
		}
		else if(strcmp(argv[i],"-lambda") == 0 && argv[i+1] != NULL) {
			packetRate_cmd = atof(argv[i+1]);
			if(packetRate_cmd < 0.1)
				packetRate_cmd = 0.1;
		}
		else if(strcmp(argv[i],"-mu") == 0 && argv[i+1] != NULL) {
			serviceRate_cmd = atof(argv[i+1]);
			if(serviceRate_cmd < 0.1)
				serviceRate_cmd = 0.1;
		}
		else if(strcmp(argv[i],"-r") == 0 && argv[i+1] != NULL) {
			tokenRate_cmd = atof(argv[i+1]);
			if(tokenRate_cmd < 0.1)
				tokenRate_cmd = 0.1;
		}
		else if(strcmp(argv[i],"-B") == 0 && argv[i+1] != NULL) {
			if(checkValidNumber(argv[i+1]))
				bucketDepth_cmd = atoi(argv[i+1]);
		}
		else if(strcmp(argv[i],"-P") == 0 && argv[i+1] != NULL) {
			if(checkValidNumber(argv[i+1]))
				tokenPerPacket_cmd = atoi(argv[i+1]);
		}
		else if(strcmp(argv[i],"-n") == 0 && argv[i+1] != NULL) {
			if(checkValidNumber(argv[i+1]))
				totalPackets_cmd = atoi(argv[i+1]);
		}
		else {
	        fprintf(stderr, "\nmalformed command");
	        fprintf(stderr, "\nusage: ./warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n");
	        exit(-1);
		}
	}


	nRemainingPackets = totalPackets_cmd;

	printf("Emulation Parameters:");
	printf("\n\tnumber to arrive = %d", totalPackets_cmd);
	if(fp == NULL) {
		printf("\n\tlambda = %.2f", packetRate_cmd);
		printf("\n\tmu = %.2f", serviceRate_cmd);
	}
	printf("\n\tr = %.2f", tokenRate_cmd);
	printf("\n\tB = %d", bucketDepth_cmd);
	if(fp == NULL) {
		printf("\n\tP = %d", tokenPerPacket_cmd);
	}
	if(fp != NULL) {
		printf("\n\ttsfile = %s\n", fileName);
	}
}


void fileRead(int argc, char **argv, My402List* queue1) {
    /*Read the file contents*/
    char buf[1026];

    while(packetNumber < totalPackets_cmd) {
    	if(fp != NULL) {
			if(fgets(buf,sizeof(buf),fp) != NULL) {
				if(!flag) { //read first line
					totalpackets = atoi(buf);
					totalPackets_cmd = totalpackets;
					flag = TRUE;
				}
				else {
					nRemainingPackets--;
					packetNumber++;//When each line is read increment the packetNumber which indicates about the packet
					fileReadLine(buf, packetNumber);
				}
			}
    	}
    	else {
    		nRemainingPackets--;
			packetNumber++;//When each line is read increment the packetNumber which indicates about the packet
			fileReadLine(buf, packetNumber);
    	}


    }
    //lock the mutex
    pthread_mutex_lock(&mutex);
    pthread_cleanup_push(cleanup_unlock_mutex, &mutex);
    packetExitFlag = TRUE;
    //pthread_cond_broadcast(&cv);
	pthread_cleanup_pop(1);
	pthread_cancel(packetThread);

}


void close_file(int fp) {
 close(fp);
}


void *packetRoutine(void *arg) {
	//pthread_cleanup_push(close_file, fp);
	//do {
		fileRead(t_argc,t_argv, queue1);

    //} while(packetNumber < totalpackets);

	//pthread_cleanup_pop(0);
	return 0;
}


void *tokenRoutine(void *arg) {
	while(!packetExitFlag || !My402ListEmpty(queue1)) {
		int overflowFlag = FALSE;
		usleep(1000000/tokenRate_cmd);
		if(nTokens >= bucketDepth_cmd) {
			printf("\n%012.3f: token t%d arrives, dropped", current_time(), ++TokenNumber);
			nTokens = bucketDepth_cmd-1;
			nDroppedTokens++;
			overflowFlag = TRUE;
		}


		pthread_mutex_lock(&mutex);
		pthread_cleanup_push(cleanup_unlock_mutex, &mutex);
		nTokens++;
		if(!overflowFlag) {
			if(nTokens==1)
				printf("\n%012.3f: token t%d arrives, token bucket now has %d token",current_time(), ++TokenNumber, nTokens);
			else
				printf("\n%012.3f: token t%d arrives, token bucket now has %d tokens",current_time(), ++TokenNumber, nTokens);
		}

		//if nummber of tokens required for packet in front in Q1 is reached then remove it and insert into Q2
		if(!My402ListEmpty(queue1)) {
			My402ListElem *firstElem = My402ListFirst(queue1);
			if(nTokens >= ((packetInfo*)firstElem->obj)->nTokens ) {
				void* tempPacketData = firstElem->obj;

				nTokens = nTokens-((packetInfo*)firstElem->obj)->nTokens;	//Decrement the total numbe of tokens
				My402ListUnlink(queue1, firstElem);       //Remove the element from Q1
				double tempCurrTime1 = current_time();
				if(nTokens==1)
					printf("\n%012.3f: p%d leaves Q1, time in Q1 = %.3fms, token bucket now has %d token", current_time(), ((packetInfo*)tempPacketData)->packetNumber, tempCurrTime1 -((packetInfo*)tempPacketData)->packetTime, nTokens);
				else
					printf("\n%012.3f: p%d leaves Q1, time in Q1 = %.3fms, token bucket now has %d tokens", current_time(), ((packetInfo*)tempPacketData)->packetNumber, tempCurrTime1 -((packetInfo*)tempPacketData)->packetTime, nTokens);

				//statisctics for Q1
				avgTimeQ1 += (tempCurrTime1 - ((packetInfo*)tempPacketData)->packetTime);


				My402ListAppend(queue2, tempPacketData);  //Insert the element to Q2
				((packetInfo*)tempPacketData)->serverTime = current_time();
				printf("\n%012.3f: p%d enters Q2", current_time(), ((packetInfo*)tempPacketData)->packetNumber);

				pthread_cond_broadcast(&cv); //broadcast to all that a packet is added to Q2
			}
		}

		pthread_cleanup_pop(1);



	}
	pthread_mutex_lock(&mutex);
	pthread_cleanup_push(cleanup_unlock_mutex, &mutex);
	tokenExitFlag = TRUE;
	pthread_cond_broadcast(&cv);

	pthread_cleanup_pop(1);
	return 0;
}






void *serverRoutine1(void* arg) {
	for(;;) {

		double sleepTime = 0;
		int pNumber;
		double tService1Actual, tTotalService;
		double serverArrivalTime;
		pthread_mutex_lock(&mutex);
		pthread_cleanup_push(cleanup_unlock_mutex, &mutex);

		//if nummber of tokens required for packet in front in Q1 is reached then remove it and insert into Q2
		//while(My402ListEmpty(queue2))
			//pthread_cond_wait(&cv, &mutex);

		while(1) {
			if(My402ListEmpty(queue2)) {

				if(tokenExitFlag){
					pthread_mutex_unlock(&mutex);

					pthread_cancel(serverThread1);
					//pthread_cancel(ctrlThread);
				}

				pthread_cond_wait(&cv, &mutex);


			}
			else
				break;
		}


		My402ListElem *firstElem = My402ListFirst(queue2);
		sleepTime = ((packetInfo*)firstElem->obj)->pServiceTime;
		pNumber = ((packetInfo*)firstElem->obj)->packetNumber;
		serverArrivalTime = ((packetInfo*)firstElem->obj)->serverTime;
		tTotalService = ((packetInfo*)firstElem->obj)->arrivalTime;



		/*statistics for Q2*/
		double tempCurrTime = current_time();
		avgTimeQ2 += (tempCurrTime-((packetInfo*)firstElem->obj)->serverTime);

		printf("\n%012.3f: p%d leaves Q2, time in Q2 = %.3fms", current_time(), pNumber, tempCurrTime-((packetInfo*)firstElem->obj)->serverTime);
		My402ListUnlink(queue2, firstElem);       //Remove the element from Q1

//
//		pthread_cleanup_pop(1);
//		pthread_mutex_lock(&mutex);
//		pthread_cleanup_push(cleanup_unlock_mutex, &mutex);

		pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, 0);
		printf("\n%012.3f: p%d begins service at S1, requesting %gms of service",current_time(), pNumber, sleepTime);
		tService1Actual = current_time();
		pthread_cleanup_pop(1);

		//sleepTime+=1;
		if(sleepTime != 0) {
			usleep(1000*sleepTime);

			 pthread_mutex_lock(&mutex);
			 pthread_cleanup_push(cleanup_unlock_mutex, &mutex)
			//statisctics for S1
			avgTimeS1 += (current_time()-serverArrivalTime);

			tService1Actual = current_time() - tService1Actual;
			//systemTimeSD[pNumber] = tService1Actual;
			tServiceTimeStatistics += tService1Actual;
			tTotalService = current_time() - tTotalService;
			tSystemTimeStatistics += tTotalService;
			squaredtSystemTimeStatistics += tTotalService*tTotalService;
			printf("\n%012.3f: p%d departs from S1, service time = %.3fms, time in system = %.3fms", current_time(), pNumber, tService1Actual, tTotalService);
			pthread_cleanup_pop(1);
			pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, 0);

		}
		if(tokenExitFlag && My402ListEmpty(queue2)) {
			pthread_cancel(serverThread1);
			pthread_cancel(ctrlThread);
		}

	}
}

void *serverRoutine2(void* arg) {
	for(;;) {

		double sleepTime = 0;
		int pNumber;
		double tService2Actual, tTotalService;
		double serverArrivalTime;
		pthread_mutex_lock(&mutex);
		pthread_cleanup_push(cleanup_unlock_mutex, &mutex);

		//if nummber of tokens required for packet in front in Q1 is reached then remove it and insert into Q2
		//while(My402ListEmpty(queue2))
			//pthread_cond_wait(&cv, &mutex);

		while(1) {
					if(My402ListEmpty(queue2)) {
						if(tokenExitFlag){
							pthread_mutex_unlock(&mutex);

							pthread_cancel(serverThread2);
							//pthread_cancel(ctrlThread);
						}

						pthread_cond_wait(&cv, &mutex);

					}
					else
						break;
				}


		My402ListElem *firstElem = My402ListFirst(queue2);
		sleepTime = ((packetInfo*)firstElem->obj)->pServiceTime;
		pNumber = ((packetInfo*)firstElem->obj)->packetNumber;
		serverArrivalTime = ((packetInfo*)firstElem->obj)->serverTime;
		tTotalService = ((packetInfo*)firstElem->obj)->arrivalTime;

		/*statistics for Q2*/
		double tempCurrTime = current_time();
		avgTimeQ2 += (tempCurrTime-((packetInfo*)firstElem->obj)->serverTime);

		printf("\n%012.3f: p%d leaves Q2, time in Q2 = %.3fms", current_time(),pNumber, tempCurrTime-((packetInfo*)firstElem->obj)->serverTime);
		My402ListUnlink(queue2, firstElem);       //Remove the element from Q1


//		pthread_cleanup_pop(1);
//		pthread_mutex_lock(&mutex);
//		pthread_cleanup_push(cleanup_unlock_mutex, &mutex);
		pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, 0);
		printf("\n%012.3f: p%d begins service at S2, requesting %gms of service",current_time(), pNumber, sleepTime);
		tService2Actual = current_time();
		pthread_cleanup_pop(1);


		//sleepTime+=1;
		if(sleepTime != 0) {
			usleep(1000*sleepTime);

			 pthread_mutex_lock(&mutex);
			 pthread_cleanup_push(cleanup_unlock_mutex, &mutex)
			//statisctics for S1
			avgTimeS2 += (current_time()-serverArrivalTime);

			tService2Actual = current_time() - tService2Actual;
			//systemTimeSD[pNumber] = tService2Actual;
			tServiceTimeStatistics += tService2Actual;
			tTotalService = current_time() - tTotalService;
			tSystemTimeStatistics += tTotalService;
			squaredtSystemTimeStatistics += tTotalService*tTotalService;
			printf("\n%012.3f: p%d departs from S2, service time = %.3fms, time in system = %.3fms", current_time(), pNumber, tService2Actual, tTotalService);
			pthread_cleanup_pop(1);
			pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, 0);
		}

		if(tokenExitFlag && My402ListEmpty(queue2)) {
			pthread_cancel(serverThread2);
			//pthread_cancel(ctrlThread);
		}
	}
}


void *ctrlThreadRoutine(void *args) {
	int sig;
	//while (1) {
		 sigwait(&set, &sig);

		 //exit(1);
		 pthread_cancel(packetThread);
		 pthread_cancel(tokenThread);


		 pthread_mutex_lock(&mutex);
		 pthread_cleanup_push(cleanup_unlock_mutex, &mutex);

		 while(!My402ListEmpty(queue2)) {
			 printf("\n%012.3f: p%d removed from Q2", current_time(), ((packetInfo*)My402ListFirst(queue2)->obj)->packetNumber);
			 My402ListUnlink(queue2, My402ListFirst(queue2));
		 }
		 while(!My402ListEmpty(queue1)) {
			 printf("\n%012.3f: p%d removed from Q1", current_time(), ((packetInfo*)My402ListFirst(queue1)->obj)->packetNumber);
			 My402ListUnlink(queue1, My402ListFirst(queue1));
		 }

		 pthread_cleanup_pop(1);
		 pthread_cancel(serverThread1);
		 pthread_cancel(serverThread2);;

	 //}
		 return 0;
}


int main(int argc, char** argv) {
	t_argc = argc;
	t_argv = argv;


    cmdLineParse(argc,argv);


	start=current_timestamp();
	printf("\n00000000.000: emulation begins");
    //printf("\n%012.3f: emulation begins", current_time());

	queue1 = (My402List*)malloc(sizeof(My402List));
    if (!My402ListInit(queue1)) {
		fprintf(stderr, "Couldn't initialize Queue1\n");
		exit(-1);

    }

    queue2 = (My402List*)malloc(sizeof(My402List));
    if (!My402ListInit(queue2)) {
		fprintf(stderr, "Couldn't initialize Queue2\n");
		exit(-1);

    }


    /*block all signals
     *
     */
    sigemptyset(&set); //put 0's to all bits in the set   000000...0
    sigaddset(&set, SIGINT); //set 1 to the SIGINT bit in set 01000...0
    sigprocmask(SIG_BLOCK, &set, 0); //make sigmask to be Union of current signal mask and set



	/*create packet thread*/
	param = NULL;
	pthread_create(&packetThread, 0, packetRoutine, &param);

	/*create token thread*/
	int x;
	pthread_create(&tokenThread, 0, tokenRoutine, &x);

	/*create server-1 thread*/
	pthread_create(&serverThread1, 0, serverRoutine1, NULL);

	/*create server-2 thread*/
	pthread_create(&serverThread2, 0, serverRoutine2, NULL);

	/*create a thread to handle Ctrl-C*/
	pthread_create(&ctrlThread, 0, ctrlThreadRoutine, NULL);


	pthread_join(tokenThread, NULL);
	pthread_join(packetThread, NULL);
	pthread_join(serverThread1, NULL);
	pthread_join(serverThread2, NULL);


	//standard deviation
	double avg=0.0, avg2=0.0;

	avg=(tSystemTimeStatistics*tSystemTimeStatistics)/(packetNumber*packetNumber);
	avg2=squaredtSystemTimeStatistics/packetNumber;



	printf("\n%012.3f: emulation ends\n", current_time());

	printf("\nStatistics:\n");
	printf("\n\taverage packet inter-arrival time = %.6g", tInterArrivalStatistics/(totalPackets_cmd*1000));
	printf("\n\taverage packet service time = %.6g\n", tServiceTimeStatistics/(totalPackets_cmd*1000));
	printf("\n\taverage number of packets in Q1 = %.6g", avgTimeQ1/(current_time()));
	printf("\n\taverage number of packets in Q2 = %.6g", avgTimeQ2/(current_time()));
	printf("\n\taverage number of packets in S1 = %.6g", avgTimeS1/(current_time()));
	printf("\n\taverage number of packets in S2 = %.6g\n", avgTimeS2/(current_time()));
	printf("\n\taverage time a packet spent in system = %.6g", tSystemTimeStatistics/(totalPackets_cmd*1000));
	printf("\n\tstandard deviation for time spent in system = %.6g\n",sqrt(avg2-avg)/1000);


	if(TokenNumber==0)
		printf("\n\ttoken drop probability = N/A: no tokens arrived at the facility");
	else
		printf("\n\ttoken drop probability = %.6g", (double)nDroppedTokens/TokenNumber);


	if(packetNumber==0)
		printf("\n\tpacket drop probability = N/A: no packet arrived at this facility\n");
	else
		printf("\n\tpacket drop probability = %.6g\n", (double)nDroppedPackets/packetNumber);

	return 0;
}

